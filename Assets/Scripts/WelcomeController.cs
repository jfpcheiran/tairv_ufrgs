﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WelcomeController : MonoBehaviour
{
    public void NextClicked()
    {
        SceneManager.LoadSceneAsync(1);
        SceneManager.UnloadSceneAsync(0);
    }

    public void changeToRadial()
    {
        SceneManager.LoadSceneAsync("TecladoRadial");
        SceneManager.UnloadSceneAsync(0);
    }
}

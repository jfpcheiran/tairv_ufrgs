﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
using System.IO;
using System;


public class KeyboardController : MonoBehaviour
{
    private string[] content = new string[7]; //ele grava vetor de strings
    public TextMeshProUGUI display;
    private int passwordsInput = 0;
    private int clearClicks = 0;

    private DateTime timeBefore;

    public ObjectController controle;


    public void ButtonClicked(int value)
    {
        if (controle.text.Length < 4)
        {
            controle.text += value.ToString();
            display.text = controle.text;
            System.Threading.Thread.Sleep(100);
        }
    }

    /*public void EnterClicked()
    {
        string year = System.DateTime.Now.Year.ToString();
        string month = System.DateTime.Now.Month.ToString();
        string day = System.DateTime.Now.Day.ToString();
        string hour = System.DateTime.Now.Hour.ToString(); //00h-23h
        string minute = System.DateTime.Now.Minute.ToString();
        string second = System.DateTime.Now.Second.ToString();
        string fileName = year+"_"+month+"_"+day+"_"+hour+"_"+minute+"_"+second;
        string filePath = Application.persistentDataPath+"/"+fileName+".txt";

        string[] content = new string[1]; //ele grava vetor de strings
        content[0] = controle.text; //gravando só a senha pra testar

        try
        {
            if (!File.Exists(filePath))
            {
                File.WriteAllLines(filePath, content); //e grava tudo de uma vez
            }
        }
        catch (System.Exception e)
        {
           
            UnityEngine.Debug.Log("Deu muito ruim na hora de gravar!" + e.ToString());
        }
    }*/

    public void EnterClicked()
    {
        /* se a senha já tem 4 dígitos */
        if (controle.text.Length == 4)
        {
            //armazena no vetor de conteúdos
            TimeSpan interval = DateTime.Now - timeBefore;
            content[(passwordsInput * 2)] = interval.TotalMilliseconds.ToString(); //tempo
            content[(passwordsInput * 2) + 1] = controle.text; //senha
            passwordsInput++;

            timeBefore = DateTime.Now;
        }

        /* se é o último clique */
        if (passwordsInput == 3)
        {
            content[6] = clearClicks.ToString(); //contagem de uso de Limpar

            //armazena todos os valores
            string year = System.DateTime.Now.Year.ToString();
            string month = System.DateTime.Now.Month.ToString();
            string day = System.DateTime.Now.Day.ToString();
            string hour = System.DateTime.Now.Hour.ToString(); //00h-23h
            string minute = System.DateTime.Now.Minute.ToString();
            string second = System.DateTime.Now.Second.ToString();
            string fileName = year + "_" + month + "_" + day + "_" + hour + "_" + minute + "_" + second;
            string filePath = Application.persistentDataPath + "/" + fileName + ".txt";

            try
            {
                if (!File.Exists(filePath))
                {
                    File.WriteAllLines(filePath, content); //e grava tudo de uma vez
                }
            }
            catch (System.Exception e)
            {
                /* e eu não sei tratar exceções em C# */
                UnityEngine.Debug.Log("Deu muito ruim na hora de gravar!" + e.ToString());
            }
            finally
            {
                SceneManager.LoadSceneAsync("removerOculus");
                SceneManager.UnloadSceneAsync(1);
            }
        }

        /* se a senha já tem 4 dígitos */
        if (controle.text.Length == 4)
        {
            //limpa tudo
            controle.text = "";
            display.text = "";
        }

    }

    public void ClearClicked()
    {
        controle.text = "";
        display.text = "";
    }

    public void changeScene(int value)
    {
        if (value == 1) { 
            SceneManager.LoadSceneAsync("Rad2D");
            //SceneManager.UnloadSceneAsync(0);
        }
        if (value == 2)
        {
            SceneManager.LoadSceneAsync("Rad2DRand");
            //SceneManager.UnloadSceneAsync(0);
        }
        if (value == 3)
        {
            SceneManager.LoadSceneAsync("Rad3D");
            //SceneManager.UnloadSceneAsync(0);
        }
        if (value == 3)
        {
            SceneManager.LoadSceneAsync("Rad3DRand");
            //SceneManager.UnloadSceneAsync(0);
        }
    }
}
